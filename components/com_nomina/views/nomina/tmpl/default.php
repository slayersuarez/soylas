<?php

/**
* Default Template for object
*
*/

// Initialize
defined('_JEXEC') or die;
$app = JFactory::getApplication(); // Joomla application
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$document = JFactory::getDocument();

$document->addStyleSheet($url.'components/com_nomina/assets/css/style.css');
$document->addStyleSheet($url.'components/com_nomina/assets/css/jquery-ui.min.css');



$user = JFactory::getUser();

if (  $user->guest  ) {
	$app->redirect('index.php/user-login');	
}

$module = JModuleHelper::getModule('mod_login');

?>

<script type="text/javascript" src="<?= $url ?>components/com_nomina/assets/js/jquery-ui.min.js"></script>



<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<div id="tabs">

	<?php $months = array(
			'1' => 'Enero',
			'2' => 'Febrero',
			'3' => 'Marzo',
			'4' => 'Abril',
			'5' => 'Mayo',
			'6' => 'Junio',
			'7' => 'Julio',
			'8' => 'Agosto',
			'9' => 'Septiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre'
		); ?>

	<ul>
	<?php 

	foreach ($this->nomina as $key => $mes) {
	?>
		<li><a href="#tabs-<?= $mes->id ?>"><?= $months[$mes->mes] ?></a></li>
	<?php
	}
	?>
	</ul>

	<?php
	foreach ($this->nomina as $key => $mes) {
	?>
		<div class="pdfs-content" id="tabs-<?= $mes->id ?>">
			<ul>
				<li><a href="<?= $url ?>pdfs/<?= $mes->filename ?>"><img src="<?= $url ?>components/com_nomina/assets/css/images/icono_pdf.png" style="width: 50px;"></a></li>
				<li><a href="<?= $url ?>pdfs/<?= $mes->filename ?>"><?= 'PDF de '.$months[$mes->mes] ?></a></li>
			</ul>
		</div>
	<?php
	}
	?>
</div>

