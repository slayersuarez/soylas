<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewNomina extends JViewLegacy {


	protected $nomina;

	// Function that initializes the view
	function display( $tpl = null ){

		$this->nomina = $this->get('Objects');
	
		parent::display( $tpl );
	
	}
	
	


}
?>