/**
*
* View for { Object }
*
*
**/

( function( $, window, document, Utilities ){
	
	var ObjectView = function( a ){
		
		// atributes, selector or global vars
		this.attribute = 'my-atribute';
		
	};
	
	ObjectView.prototype ={
			

			/**
			 * Function header when calls the model method and gives the view the response
			 *
			 * @param {}
			 * @return {}
			 */
			onCompleteAction: function( data ){
				
				// do stuff
				console.log( data );
			}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.ObjectView = new ObjectView();
	
})( jQuery, this, this.document, this.Misc, undefined );