<?php

/*******************************************************************************************/
/*
/*		Web: http://www.asdesigning.com
/*		Web: http://www.astemplates.com
/*		License: GNU/GPL
/*
/*******************************************************************************************/

include_once 'fonts.php';

$font_size1	 				= $params->get('font_size1');
if($font_size1)
	$line_height1			= 'line-height: ' . $font_size1 + 2 . 'px;';

$font_size2	 				= $params->get('font_size2');
if($font_size2)
	$line_height2			= 'line-height: ' . $font_size2 + 2 . 'px;';

$submenu_width				= $params->get('submenu_width');

$font_family 				= $params->get('font_family');
$google_font 				= array('fontlink'=>false, 'fontfamily'=>false);
$google_font 				= googleFontChooser($font_family);

$font_family 				= $google_font['fontfamily'];

if($google_font['fontlink'])
{
	echo $google_font['fontlink'];
}
		
?>

<style type="text/css">

#as-menu ul.as-menu > li > a,
#as-menu ul.as-menu > li > span
{
	font-size: <?php echo $font_size1 ?>px;
	<?php echo $line_height1 ?>
	<?php echo $google_font['fontfamily']; ?>
}

#as-menu ul.as-menu ul
{
	width: <?php echo $submenu_width ?>px;
}

#as-menu ul.as-menu ul li a,
#as-menu ul.as-menu ul li span
{
	font-size: <?php echo $font_size2 ?>px;
	<?php echo $line_height2 ?>
	<?php echo $google_font['fontfamily']; ?>
}

#as-menu ul.as-menu li li:hover ul,
#as-menu ul.as-menu li li.asHover ul,
#as-menu ul.as-menu li li li:hover ul,
#as-menu ul.as-menu li li li.asHover ul
{
	left: <?php echo $submenu_width ?>px;
}


</style>