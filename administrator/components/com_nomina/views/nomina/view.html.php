<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class NominaViewNomina extends JViewLegacy {

	
	// Function that initializes the view
	function display( $tpl = null ){

		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
	
		// Add the toolbar with the actions
		$this->addToolbar();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::save( "nomina.save" );
		JToolBarHelper::cancel( "cancel" );
	}

}
?>