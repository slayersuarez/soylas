<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.js');
$document->addStyleSheet($host.'administrator/components/com_nomina/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_nomina/assets/css/style.css');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/misc/misc.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/views/certificacion.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/models/certificacion.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/controllers/certificacion.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/handlers/certificacion.js');
$document->addScript( $host.'administrator/components/com_nomina/assets/js/misc/file.js');


?>

<fieldset>
	<legend> Cargar PDFs de Nomina</legend>

	<form action="<?php echo JRoute::_('index.php?option=com_nomina');?>" method="post" name="adminForm" id="adminForm">

	<fieldset class="adminform">

		<div class="hero-unit">

			<p>Seleccione el mes en el que desea cargar la nómina</p>

			<select name="mes">
				<option>Seleccione</option>
				<option value="1">Enero</option>
				<option value="2">Febrero</option>
				<option value="3">Marzo</option>
				<option value="4">Abril</option>
				<option value="5">Mayo</option>
				<option value="6">Junio</option>
				<option value="7">Julio</option>
				<option value="8">Agosto</option>
				<option value="9">Septiembre</option>
				<option value="10">Octumbre</option>
				<option value="11">Noviembre</option>
				<option value="12">Diciembre</option>
			</select>
			<legend>Archivo PDF</legend>
			
			<div id="pdf-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			</noscript>         
		</div>

		<div class="pdf-upload-status">
			<span class="message">No ha seleccionado ningun PDF aún.</span>
			<ul class="pdf-upload-status-list">
				<li>
					<div class="pdf-status-bar">
						<span class="bar-inside"></span>
						<span class="bar-message"></span>
					</div>
				</li>
				<li>
					<span class="pdf-total-title"></span>
					<div class="pdf-total-bar">
						<span class="bar-inside"></span>
						<span class="bar-message"></span>
					</div>
				</li>
				<li>
					<ul class="pdf-error-list"></ul>
				</li>
			</ul>
		</div>

		<div class="pdf-log">
			<div class="errors">Errores: <span class="pdf-errors">0</span></div>
			<div class="correctos">Correctos: <span class="pdf-correctos">0</span></div>
		</div>

		</div>
	</fieldset>

	<input type="hidden" name="option" value="com_nomina" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="" />
	<input type="hidden" name="filename" value />
	
</form>
</fieldset>