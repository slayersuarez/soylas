/**
*
* File uploading
*
*/

( function( $, window, document, certificacion ){

	var Uploader = function( a ){

		this.PDFparams = {

				element: document.getElementById( 'pdf-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_nomina',
			    	task: 'nomina.uploadPdfs'
			    }

			,   allowedExtensions: [ 'pdf' ]
			,   debug: false
			,	multiple: false
			,	onSubmit: this.onSubmitPDF
			,	onProgress: this.onProgressPDF
			,	onComplete: this.onCompletePDF
			,   onError: this.onErrorPDF
		    
		};


	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploaderPDF();
			}

		,	createUploaderPDF: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.PDFparams );

			}

		,	onSubmitPDF: function( id, fileName ){

				// Reset all strings
				$( '.pdf-upload-status' ).find( '.message' ).text( '' );
				$( '.pdf-total-title' ).text( '' );
				$( '.pdf-status-bar' ).find( '.bar-inside' ).css( 'width', '0%' );
				$( '.pdf-total-bar' ).find( '.bar-inside' ).css( 'width', '0%' );

				window.Uploader.pdfQueue = 0;
				window.Uploader.pdfUploadCount = 1;

				var queue = $( "input[name='file']" )[0].files;

				if( window.Uploader.pdfQueue == 0 ){
					
					window.Uploader.pdfQueue = queue.length;

					$( '.pdf-upload-status' ).find( '.message' ).text(
						'Subiendo PDFs 1 de ' + queue.length 
					);

				}

				$( '.pdf-upload-status-list' ).show();

			}

		,	onProgressPDF: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				var progressBar = $( '.pdf-status-bar' ).find( '.bar-inside' )
				,	progressMessage = $( '.pdf-status-bar' ).find( '.bar-message' );

				progressBar.css( 'width', perc + '%' );
				progressMessage.text( fileName + ' (' + perc + '%)' );

			}

		,	onCompletePDF: function(id, fileName, responseJSON){

				var count = window.Uploader.pdfUploadCount++;
				var perc = Math.floor( count / window.Uploader.pdfQueue * 100 );

				var totalTitle = $( '.pdf-total-title' )
				,	progressBar = $( '.pdf-total-bar' ).find( '.bar-inside' )
				,	progressMessage = $( '.pdf-total-bar' ).find( '.bar-message' );

				progressBar.css( 'width', perc + '%' );
				progressMessage.text( '(' + perc + '%)' );
				totalTitle.text( 'Total (' + count + '/' + window.Uploader.pdfQueue + ')' );

				$( '.pdf-upload-status' ).find( '.message' ).text(
					'Cargando PDFS ' + count + ' de ' + window.Uploader.pdfQueue 
				);

				$('input[name="filename"]').val( responseJSON.filename );

				if( perc == 100 ){
					progressBar.addClass('success');
					return;
				}


			}

		,   onErrorPDF: function(id, fileName, xhr){

				console.log( xhr );
				
				var li = $('<li>');

				li.text( 'Error: Archivo' + fileName + ' no cargado correctamente.'  );

				li.appendTo('.pdf-error-list');

				$('.pdf-error-list').show();
			}

		
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, this.CertificacionController, undefined );







