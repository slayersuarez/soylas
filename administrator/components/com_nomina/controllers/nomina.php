
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class NominaControllerNomina extends JControllerLegacy{


	/**
	*
	* Uploads pdf files
	*
	*/
	public function uploadPdfs(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'pdf' );
		// max file size in bytes
		$sizeLimit = 1 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../pdfs/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}

	/**
	*
	* Save Pdf Filename 
	*
	*/
	public function save(){

		$jinput = JFactory::getApplication()->input;
		$app = JFactory::getApplication();

		$mes = $jinput->get('mes');
		$filename = $jinput->get('filename');

		$months = array(
			'1' => 'Enero',
			'2' => 'Febrero',
			'3' => 'Marzo',
			'4' => 'Abril',
			'5' => 'Mayo',
			'6' => 'Junio',
			'7' => 'Julio',
			'8' => 'Agosto',
			'9' => 'Septiembre',
			'10' => 'Octubre',
			'11' => 'Noviembre',
			'12' => 'Diciembre'
		);

		if ($mes == '') {
			$app->redirect( 'index.php?option=com_nomina', 'Seleccione un mes.', 'error', false );
		}

		if ($filename == '') {
			$app->redirect( 'index.php?option=com_nomina', 'Seleccione un PDF.', 'error', false );
		}

		$model = $this->getModel('nomina');

		

		$max = $model->selectMax();

		$maximo = intval($max->maximo) + 1;

		if( $maximo != intval($mes) ){
			$app->redirect( 'index.php?option=com_nomina', 'Debe cargar el PDF del mes de '.$months[$maximo], 'error', false );
		}
		
		$pdfs = $model->getObjects();

		if ( count($pdfs) >= 3 ) {

			$mindate = $model->selectMin();
			$model->getMinDate( intval($mindate->minimo) );

		}

		$meses = $model->getMes( $mes );

		$id = NULL;

		if ( ! empty( $meses ) ) {

			$id = $meses->id;
		}

		$args = array( 'id' => $id, 'mes' => $mes, 'filename' => $filename, 'fecha' => date('Y-m-d') );

		$model->instance( $args );

		if ( ! $model->save( 'bool' ) ) {
			
			$app->redirect( 'index.php?option=com_nomina', 'No se pudo guardar el PDF', 'error', false );
		}

		

		//
		if( $id != null ){
			$app->redirect( 'index.php?option=com_nomina', 'PDF de Nomina del mes '. $months[$mes] .' sobreescrito.', 'message', false );
		}else{
			$app->redirect( 'index.php?option=com_nomina', 'PDF de Nomina guardado.', 'message', false );
		}
		
	
	}
}
?>