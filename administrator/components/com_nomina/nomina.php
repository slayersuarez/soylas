<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT .  '/controller.php' );
require_once( JPATH_COMPONENT . '/helpers/uploader.php' );

$controller = JControllerLegacy::getInstance('nomina');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>